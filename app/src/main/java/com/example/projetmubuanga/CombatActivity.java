package com.example.projetmubuanga;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.example.projetmubuanga.arena.Door;
import com.example.projetmubuanga.arena.DoorState;
import com.example.projetmubuanga.character.Player;
import com.example.projetmubuanga.engine.Game;

public class CombatActivity extends AppCompatActivity {
    private Game game;
    private int doorPosition;
    private TextView vPowerPlayer;
    private TextView vHealthPlayer;
    private TextView vPowerOpponent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_combat);
        System.out.println();

        // initialiser les valeurs
        this.vPowerPlayer = findViewById(R.id.vPuiss);
        this.vHealthPlayer = findViewById(R.id.vPV);
        this.vPowerOpponent = findViewById(R.id.advPuiss);

        Intent intention = getIntent() ;
        this.game = intention.getParcelableExtra(DonjonAdapter.GAME) ;
        this.doorPosition = intention.getIntExtra(DonjonAdapter.POSITION,0) ;
        Player player = this.game.getPlayer();
        int powerOpponent = this.game.getDonjon().getDoors().get(doorPosition).getOpponent().getPower();
        this.vPowerPlayer.setText(player.getPower()+"");
        this.vHealthPlayer.setText(player.getHealth()+"");
        this.vPowerOpponent.setText(powerOpponent+"");
    }

    /**
     * Fonction appeler lorsqu'on clique sur le bouton attaque
     * @param view
     */
    public void onAttack(View view) {

        // effectue l'action de l'attaquer
        this.game.attack(this.doorPosition);
        this.game.getDonjon().setUnvisited(this.game.getDonjon().getUnvisited() - 1);
        // créer l'intent résultat
        Intent resultat = new Intent();
        // ajout des résultats
        resultat.putExtra(DonjonAdapter.GAME, this.game) ;
        resultat.putExtra(DonjonAdapter.RES, "WIN") ;
        // prépare le retour des résultats
        setResult(RESULT_OK, resultat);
        finish();
    }


    public void onFuite(View view) {
        // effectue l'action de fuire
        this.game.fuite(this.doorPosition);
        this.game.getPlayer().setHealth(this.game.getPlayer().getHealth() -1);
        this.game.getDonjon().setUnvisited(this.game.getDonjon().getUnvisited() - 1);
        // créer l'intent résultat
        Intent resultat = new Intent();
        resultat.putExtra(DonjonAdapter.RES, "LOSE") ;
        resultat.putExtra(DonjonAdapter.GAME, this.game) ;
        setResult(RESULT_OK, resultat);
        finish();
    }
}

package com.example.projetmubuanga.arena;

import android.os.Build;
import android.os.Parcel;
import android.os.Parcelable;

import androidx.annotation.RequiresApi;

import com.example.projetmubuanga.character.Opponent;

import java.util.Random;

/**
 * Salle de créarion d'une pièce
 */
public class Door implements Parcelable {
    private Opponent opponent;
    private DoorState doorState;
    private int position;

    public Door(Boolean bonus) {

        Random random = new Random();
        int power = 0;
        power = random.nextInt(150) + 1;
        this.doorState = DoorState.UNVISITED;
        this.opponent = new Opponent(power);
    }

    protected Door(Parcel in) {
        opponent = in.readParcelable(Opponent.class.getClassLoader());
        String tmpState = in.readString();
        position = in.readInt();
        doorState = DoorState.valueOf(tmpState);
    }

    public static final Creator<Door> CREATOR = new Creator<Door>() {
        @Override
        public Door createFromParcel(Parcel in) {
            return new Door(in);
        }

        @Override
        public Door[] newArray(int size) {
            return new Door[size];
        }
    };

    public Opponent getOpponent() {
        return opponent;
    }

    public void setOpponent(Opponent opponent) {
        this.opponent = opponent;
    }


    public DoorState getDoorState() {
        return doorState;
    }

    public void setDoorState(DoorState doorState) {
        this.doorState = doorState;
    }

    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeParcelable(opponent,0);
        parcel.writeString(doorState.name());
        parcel.writeInt(position);
    }
}

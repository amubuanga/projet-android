package com.example.projetmubuanga.arena;

/**
 * Les différents états d'une Door (pièce)
 */
public enum DoorState {
    WIN, LOSE, ESCAPE, UNVISITED
}

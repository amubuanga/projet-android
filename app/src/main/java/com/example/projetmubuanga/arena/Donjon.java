package com.example.projetmubuanga.arena;

import android.os.Parcel;
import android.os.Parcelable;

import com.example.projetmubuanga.character.Opponent;

import java.util.*;

/**
 * Class qui contient le Donjon avec ses 16 pièces (@Door)
 */
public class Donjon implements Parcelable {
    private List<Door> doors;
    private static final int SIZE = 16;
    private int unvisited;

    public Donjon() {
        this.unvisited = 16;
        this.doors = new ArrayList<Door>();
        List<Integer> doorsHasBonus = new ArrayList<Integer>();
        for (int i = 0; i < SIZE; i++) {
           this.doors.add(new Door(false));
        }
    }

    public int getUnvisited() {
        return unvisited;
    }

    public void setUnvisited(int unvisited) {
        this.unvisited = unvisited;
    }

    protected Donjon(Parcel in) {

        doors = in.createTypedArrayList(Door.CREATOR);
        unvisited = in.readInt();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeTypedList(doors);
        dest.writeInt(unvisited);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<Donjon> CREATOR = new Creator<Donjon>() {
        @Override
        public Donjon createFromParcel(Parcel in) {
            return new Donjon(in);
        }

        @Override
        public Donjon[] newArray(int size) {
            return new Donjon[size];
        }
    };

    public List<Door> getDoors() {
        return doors;
    }

    public void setDoors(List<Door> doors) {
        this.doors = doors;
    }
}

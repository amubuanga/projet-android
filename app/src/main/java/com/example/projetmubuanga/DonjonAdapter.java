package com.example.projetmubuanga;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.projetmubuanga.arena.Donjon;
import com.example.projetmubuanga.arena.DoorState;
import com.example.projetmubuanga.engine.Game;

public class DonjonAdapter extends BaseAdapter {
    public final static String GAME = "com.example.projetmubuanga.GAME" ;
    public final static String RES = "com.example.projetmubuanga.RES" ;
    public final static String POSITION = "com.example.projetmubuanga.POSITION" ;

    private Activity activity;
    private LayoutInflater inflater;
    private String[] numItem;
    private int[] doorImages;
    private Game game;

    /**
     * Initialisation de l'adapter
     * @param activity
     * @param numItem
     * @param doorImages
     * @param game
     */
    public DonjonAdapter(Activity activity, String[] numItem, int[] doorImages, Game game) {
        this.activity = activity;
        this.numItem = numItem;
        this.doorImages = doorImages;
        this.game = game;
    }

    @Override
    public int getCount() {
        return numItem.length;
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(final int i, View view, ViewGroup viewGroup) {
        if (inflater == null){
            inflater = (LayoutInflater) activity.getSystemService(activity.LAYOUT_INFLATER_SERVICE);
        }
        if(view == null){
            view = inflater.inflate(R.layout.door, null);
        }

        ImageView imageView = view.findViewById(R.id.doorImage);
        DoorState doorState = this.game.getDonjon().getDoors().get(i).getDoorState();
        if (doorState == DoorState.WIN){
            imageView.setImageResource(R.drawable.win);
        }else if (doorState == DoorState.LOSE) {
            imageView.setImageResource(R.drawable.die);
        }else{
            imageView.setImageResource(doorImages[i]);
        }
        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                combatView(view, i);
            }
        });
        return view;
    }

    /**
     * Cette methode nous renvoie vers l'activité combat
     * @param view
     * @param i
     */
    public void combatView(View view, int i) {

        // si le joueur a encore des points de vie
        final int health = this.game.getPlayer().getHealth();
        if (health > 0){
        // si la pièce n'est pas visiter, on peut aller au combat
            if (this.game.getDonjon().getDoors().get(i).getDoorState() == DoorState.UNVISITED){
                Toast.makeText(activity, "Vous entrez dans l'arène "+ numItem[+i],Toast.LENGTH_SHORT).show();
                Intent intent = new Intent(this.activity, CombatActivity.class);
                intent.setAction(Intent.ACTION_GET_CONTENT);
                intent.putExtra(DonjonAdapter.GAME, this.game) ;
                intent.putExtra(DonjonAdapter.POSITION, i) ;
                this.activity.startActivityForResult(intent,MainActivity.GO_COMBAT);
            }else {
                Toast.makeText(activity, "Vous ête déjà entrer dans cette pièce",Toast.LENGTH_SHORT).show();
            }
        }

    }
}

package com.example.projetmubuanga.engine;

import android.os.Parcel;
import android.os.Parcelable;

import com.example.projetmubuanga.arena.Donjon;
import com.example.projetmubuanga.arena.DoorState;
import com.example.projetmubuanga.character.Player;

import java.util.Random;

/**
 * Le moteur de jeu
 */
public class Game implements Parcelable {
    private Donjon donjon;
    private Player player;

    public Game(Donjon donjon, Player player) {
        this.donjon = donjon;
        this.player = player;
    }

    protected Game(Parcel in) {
        this.donjon = in.readParcelable(Donjon.class.getClassLoader());
        this.player = in.readParcelable(Player.class.getClassLoader());
    }

    public static final Creator<Game> CREATOR = new Creator<Game>() {
        @Override
        public Game createFromParcel(Parcel in) {
            return new Game(in);
        }

        @Override
        public Game[] newArray(int size) {
            return new Game[size];
        }
    };

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeParcelable(donjon, 0);
        dest.writeParcelable(player, 1);
    }

    @Override
    public int describeContents() {
        return 0;
    }


    public Donjon getDonjon() {
        return donjon;
    }

    public void setDonjon(Donjon donjon) {
        this.donjon = donjon;
    }

    public Player getPlayer() {
        return player;
    }

    public void setPlayer(Player player) {
        this.player = player;
    }

    public void attack(int i) {
        int playerPower = this.getPlayer().getPower();
        Random rand = new Random();
        int randp =  rand.nextInt(2);
        int rando =  rand.nextInt(2);
        int  opponentPower = this.getDonjon().getDoors().get(i).getOpponent().getPower();
        int res =  (playerPower * randp) - (opponentPower * rando);

        if (res < 0) {
            int health = this.getPlayer().getHealth();
            this.getPlayer().setHealth(health - 1);
            this.getDonjon().getDoors().get(i).setDoorState(DoorState.LOSE);
        }else {
            this.getDonjon().getDoors().get(i).setDoorState(DoorState.WIN);
        }
    }

    public void fuite(int doorPosition) {
        this.getDonjon().getDoors().get(doorPosition).setDoorState(DoorState.LOSE);
    }
}

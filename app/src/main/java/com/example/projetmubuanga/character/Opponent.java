package com.example.projetmubuanga.character;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * la classe Opponent (adversaire), qui hérite de Character
 */

public class Opponent extends Character implements Parcelable {
    public Opponent(int power) {
        super(power);
    }

    protected Opponent(Parcel in) {
        this.power = in.readInt();
    }

    public static final Creator<Opponent> CREATOR = new Creator<Opponent>() {
        @Override
        public Opponent createFromParcel(Parcel in) {
            return new Opponent(in);
        }

        @Override
        public Opponent[] newArray(int size) {
            return new Opponent[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(power);
    }
}

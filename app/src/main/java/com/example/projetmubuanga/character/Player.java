package com.example.projetmubuanga.character;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Classe du joueur
 */

public class Player extends Character implements Parcelable {
    private int health;
    public Player() {
        super();
        this.power = 100;
        this.health = 10;
    }


    protected Player(Parcel in) {
        power = in.readInt();
        health = in.readInt();
    }

    public int getHealth() {
        return health;
    }

    public void setHealth(int health) {
        this.health = health;
    }

    public static final Creator<Player> CREATOR = new Creator<Player>() {
        @Override
        public Player createFromParcel(Parcel in) {
            return new Player(in);
        }

        @Override
        public Player[] newArray(int size) {
            return new Player[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(power);
        parcel.writeInt(health);
    }
}

package com.example.projetmubuanga.character;

/**
 * Classe de création d'un personnage (joueur, adversaire,...)
 */
public class Character {
    protected int power;

    public Character() {
    }

    public Character(int power) {
        this.power = power;
    }

    public int getPower() {
        return power;
    }

    public void setPower(int power) {
        this.power = power;
    }
}

package com.example.projetmubuanga;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.projetmubuanga.arena.Donjon;
import com.example.projetmubuanga.character.Player;
import com.example.projetmubuanga.engine.Game;

/**
 * L'activité principale du jeu
 */
public class MainActivity extends AppCompatActivity {
    public static final int GO_COMBAT = 10;
    public final static String PUISS = "fr.ulco.renaud.appli.PUISS" ;
    private Game game;
    private GridView donjonView;
    private TextView pVie;
    private TextView vPne;
    private TextView vp;
    private TextView resValue;

    String[] numItem = {
            "01","02","03","04",
            "05","06","07","08",
            "09","10","11","12",
            "13","14","15","16"
    };

    // Tableau des images qui correspondent aux differents adversaires
    int[] doorImages = {
            R.drawable.p1,R.drawable.p2,R.drawable.p3,R.drawable.p4,
            R.drawable.p5,R.drawable.p6,R.drawable.p7,R.drawable.p8,
            R.drawable.p9,R.drawable.p10,R.drawable.p11,R.drawable.p12,
            R.drawable.p13,R.drawable.p14,R.drawable.p6,R.drawable.p2
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Intent intention = getIntent() ;
        Game parcelableExtra = intention.getParcelableExtra(DonjonAdapter.GAME);
        if (parcelableExtra == null) {
            this.game = new Game(new Donjon(), new Player());
        } else {
            this.game = parcelableExtra;
        }

        // Initialisation des différentes valeurs
        this.pVie = findViewById(R.id.pVie);
        this.vPne = findViewById(R.id.vPne);
        this.vp = findViewById(R.id.vp);
        this.resValue = findViewById(R.id.resValue);

        Player player = this.game.getPlayer();
        pVie.setText(player.getHealth()+"");
        vp.setText(player.getPower()+"");
        vPne.setText(this.game.getDonjon().getUnvisited()+"");
        donjonView = findViewById(R.id.donjonView);

        DonjonAdapter adapter = new DonjonAdapter(MainActivity.this,numItem, doorImages,this.game);

        donjonView.setAdapter(adapter);
    }



    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu, menu);
        return true;
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        switch (item.getItemId())
        {
            case R.id.recommencer:
                this.recommencer();
                return true;

            case R.id.quitter:
                System.exit(0);
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    /**
     * Recommencer la partie
     */
    private void recommencer() {
        this.game = null;
        recreate();

    }

    /**
     * La fonction qui récupère le resultat de la deuxième activité et de la mise à jour des infos
     * @param requestCode
     * @param resultCode
     * @param data
     */
    @Override
    protected void onActivityResult(int requestCode, int resultCode,@Nullable Intent data) {
        if (requestCode == GO_COMBAT) {
            // On vérifie aussi que l'opération s'est bien déroulée
            if (resultCode == RESULT_OK) {
                Game parcelableExtra = data.getParcelableExtra(DonjonAdapter.GAME);
                if (parcelableExtra == null) {
                    this.game = new Game(new Donjon(), new Player());
                } else {
                    this.game = parcelableExtra;
                }


                // On actualise les valeurs dans le main
                this.vPne.setText(this.game.getDonjon().getUnvisited()+"");
                this.pVie.setText(this.game.getPlayer().getHealth()+"");
                String res = data.getStringExtra(DonjonAdapter.RES);
                if(res == "WIN"){
                    resValue.setText("VOUS AVEZ GAGNE");
                }else{
                    resValue.setText("VOUS AVEZ PERDU");
                }


                DonjonAdapter adapter = new DonjonAdapter(MainActivity.this,numItem, doorImages,this.game);
                donjonView.setAdapter(adapter);
            }
        }
    }

}
